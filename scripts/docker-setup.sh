#!/bin/sh
if [ ! -d .local/bin ] ; then
	mkdir -p .local/bin
	touch .local/bin/test
fi
false
if [ $? -eq 0 ] ; then
	echo "A"
else
	echo "B"
fi
if [ ! -f .local/bin/docker-cli ] ; then
	hash apt-get 2> /dev/null
	if [ $? -eq 0 ] ; then
		apt-get update && apt-get download docker.io && dpkg --fsys-tarfile docker.io_*.deb | tar xOf - ./usr/bin/docker > ./docker-cli && chmod +x ./docker-cli && rm docker.io_*.deb
		mv docker-cli .local/bin/
	else
		echo "Not running on debian compatible distribution" 2>&1
		exit 1
	fi
fi
