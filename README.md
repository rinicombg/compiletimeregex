# Compile Time Regular Expressions

Compile to machine instructions rather than use the REGEX VM supplied with
C++.

# Building

This project uses meson and has a gitlab-ci configuration that will create an
image, it can use the docker daemon on the machine that hosts docker.

## GitLab Runner Configuration

To allow a build to interact with docker the container must first contain an
executable that can interact with the deamon and the container must have access
to the daemon socket on the host.

Example Runner Configuration:

```
concurrent = 1
check_interval = 0
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "bob"
  url = "https://gitlab.com"
  id = ...
  token = "glrt-..."
  token_obtained_at = 2024-04-08T09:55:35Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "docker"
  [runners.cache]
    MaxUploadedArchiveSize = 0
  [runners.docker]
    tls_verify = false
    image = "alpine:3.19"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
    shm_size = 0
    network_mtu = 0
```

The important things to note is the attributes are inherited and normally the
image will be overwritten. Docker is available to via the volume mapping of
the docker socket: `"/var/run/docker.sock:/var/run/docker.sock"`. Of course
this is a major security problem since docker effectively has root access to
the host machine, if this is a concern run in a VM or make other provisions.

## Trigger Build from API

`ref` is a branch or tag, `<project_id>`: to find this go to Settings ->
General in the project, `token` must be generated see references.

```
curl --request POST --form token=glptt-... --form ref=main "https://gitlab.com/api/v4/projects/<project_id>/trigger/pipeline"
```

## References

* [GitLab Runner Advanced Configuration](https://docs.gitlab.com/runner/configuration/advanced-configuration.html)

* [Trigger Pipeline via API](https://docs.gitlab.com/ee/ci/triggers/)

