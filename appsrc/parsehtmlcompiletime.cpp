#include <iostream>
#include <ctre.hpp>
#include <cstdio>
#include <cstring>

static constexpr auto pattern = ctll::fixed_string{ "<\\s*(/?)\\s*(\\w+)([^>]*)>" };

void process_html_buffer(const char * inputStr, size_t inputStrSz) {
	std::string_view input(inputStr, inputStrSz);
	const char * startFrom = inputStr;
	while (ctre::regex_results match = ctre::search<pattern>(input)) {
		std::string_view completeMatch = match.get<0>().to_view();
		//fprintf(stdout, "%.*s\n", static_cast<int>(completeMatch.length()), static_cast<const char *>(completeMatch.data()));
		std::cout << completeMatch << "\n";
		input.remove_prefix(static_cast<const char *>(completeMatch.data()) - static_cast<const char *>(input.data()) + completeMatch.length());
	}
}

int main(int argc, char *argv[]) {
	const char * input = R"(something before the first anchor tag< a 
	href="url goes here"> This is the link content     <b>What? A Link?</b >, yes... a link          </ a>)";
	//const char * input = "123, 456";
	process_html_buffer(input, (size_t)strlen(input));
}

