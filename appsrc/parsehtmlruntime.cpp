#include <iostream>
#include <regex>
#include <cstdio>
#include <cstring>

void process_html_buffer(const char * inputStr, size_t inputStrSz) {
	std::regex pattern("<\\s*(/?)\\s*(\\w+)([^>]*)>");
	const char * searchStart = inputStr;
	const char * searchEnd = inputStr + inputStrSz;
	std::cmatch match;
	while (std::regex_search(searchStart, searchEnd, match, pattern)) {
		//std::string myMatch = match[2];

		//fprintf(stdout, "%s\n", match[0].str().c_str());
		fprintf(stdout, "%d %d\n", (int)match[1].length(), (int)match[2].length());
		//fprintf(stdout, "%s\n", myMatch.c_str());

		searchStart = match.suffix().first;
	}
}

int main(int argc, char *argv[]) {
	const char * input = R"(< ab
	href=\"\"> This is the link content     <b>What? A Link?</b >, yes... a link          </ a>)";
	process_html_buffer(input, (size_t)strlen(input));
}

